# INTRODUCTION

This repository contains scripts to build and release a Dawn node addon.

## System Requirements

See [Building Dawn](https://dawn.googlesource.com/dawn/+/HEAD/docs/building.md)
for system requirements. Although not listed, `go` may also be required.

## Getting The Code

To clone the
[Dawn Build repository](https://gitlab.com/Distributed-Compute-Protocol/dawn-build),
enter the following:
```
git clone --recursive git@gitlab.com:Distributed-Compute-Protocol/dawn-build.git
```

## Building

To build, enter the following:
```
mkdir build
cd build
cmake .. [<CMake options>]
cmake --build .
```

To use a local [Dawn repository](https://github.com/Distributive-Network/dawn)
clone, specify CMake option `-D DAWN_ROOT=<directory>`. **NOTE:**

* Do not use `--recursive` when cloning; the submodules are huge, unneeded, and
some require a Chromium developer account.
* When switching branches, the submodule commits can be synced by rerunning
`cmake .` in the `dawn-build` build directory.

## Deploying

To deploy, clone
[dcp-native-ci](https://gitlab.com/Distributed-Compute-Protocol/dcp-native-ci)
and read the "Deploying" section of the "README.md" file therein.
